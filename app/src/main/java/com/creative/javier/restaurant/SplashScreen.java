package com.creative.javier.restaurant;

import android.app.Activity;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.splash_screen);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                cambiarActivity();
            }
        },4000);
    }

    private void cambiarActivity(){
        startActivity(new Intent(this, MainActivity.class));
        this.finish();
    }
}
